<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Film extends Model {
    protected $casts = [
        "rental_rate"       => 'float'
    ];
}