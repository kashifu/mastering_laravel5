<?php
/**
 * Created by PhpStorm.
 * User: Kashif.U
 * Date: 9/1/2016
 * Time: 10:37 PM
 */

namespace App\Repositories;

use App\User;

class TaskRepository {

    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->tasks()
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Store a User's task.
     * @param $request
     */
    public function saveUserTask($request) {
        $request->user()->tasks()->create([
            'name' => $request->name,
        ]);
    }
}