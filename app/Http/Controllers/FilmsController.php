<?php

namespace App\Http\Controllers;
use App\Repositories\FilmsRepository;

class FilmsController extends Controller {

    private $film;

    public function __construct(FilmsRepository $film) {
        $this->film = $film;
    }

    public function index() {
        return \Response::json($this->film->all());
    }
}