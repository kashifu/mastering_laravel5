<?php

namespace App\Http\Controllers;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Services\TaskService;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{
    /**
     * The task service instance.
     *
     * @var TaskService
     */
    protected $tasks;

    /**
     * The task Repository instance.
     *
     * @var TaskRepository
     */
    protected $repo;

    public function __construct(TaskService $tasks, TaskRepository $repo) {
        $this->middleware('auth');
        $this->tasks = $tasks;
        $this->repo = $repo;
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {
        $tasks = $this->tasks->forUser($request->user());

        return view('tasks.index', [
           'tasks' => $tasks,
        ]);
    }

    /**
     * Create a new task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $this->repo->saveUserTask($request);

        return redirect('/tasks');
    }

    public function destroy(Request $request, Task $task) {
        $this->authorize('destroy', $task);
        $task->delete();
        return redirect('/tasks');
    }
}
